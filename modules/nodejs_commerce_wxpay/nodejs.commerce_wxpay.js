
(function ($) {

Drupal.Nodejs.callbacks.nodejsCommerceWXPay = {
  callback: function (message) {
    if (message.payed == true) {
      $('#commerce-wxpay-payment-form').submit();
    }
  }
};

}(jQuery));
